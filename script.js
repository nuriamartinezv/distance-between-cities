let cities = [
  {name: 'Valencia', x: -0.376288, y: 39.469907},
  {name: 'Los Angeles', x: -118.243685, y: 34.052234},
  {name: 'Madrid', x: -3.703790, y: 40.416775},
  {name: 'Frankfurt', x: 8.682127, y: 50.110922},
  {name: 'New York', x: -74.005973, y: 40.712775},
  {name: 'Buenos Aires', x: -58.381559, y: -34.603684},
  {name: 'Nuremberg', x: 11.076665, y: 49.452102},
  {name: 'Tokyo', x: 139.691706, y: 35.689487},
  {name: 'Moscow', x: 37.617300, y: 55.755826},
  {name: 'Reykjavík', x: -21.817439, y: 64.126521},
  {name: 'Beijing', x: 116.407396, y: 39.904200},
];
let buttons = [];

for (var i = 0; i < cities.length; i++) {
  buttons[i] = `
    <button type="button" name="button" class="button">`
    + cities[i].name + `</button>
    `;
}

$('.cities-buttons').html(buttons);

let cityButton = '';
let selectedCity = [];
let cityIndex = [];
let dist;


$('.button').on('click', function() {
  console.log("hello");
  if (selectedCity.length < 2) {
    cityButton = this;
    if (selectedCity.length === 0 ) {
      cityIndex.push($(this).index());
      selectedCity.push(cities[cityIndex[0]]);
    } else if (selectedCity.length === 1){
      cityIndex.push($(this).index());
      if (cityIndex[1] < cityIndex[0]) {
        cityIndex.pop();
        cityIndex.push($(this).index());
        selectedCity.push(cities[cityIndex[1]]);
      } else {
        cityIndex.pop();
        cityIndex.push($(this).index() + 1);
        selectedCity.push(cities[cityIndex[1]]);
      }
    }

    $('.selected-cities').append(cityButton);
  }
  if (selectedCity.length === 2) {
    let units = 'km';
    let renderResult = `
    <p>In a straight line, the distance between
    ${selectedCity[0].name} and ${selectedCity[1].name}
    is ${calculateDistance()} ${units}</p>
    `;
    let resetButton = `
      <button type="button" name="button" class="reset">
      RESET</button>`;

    $('.result').html(renderResult);
    $('.reset').html(resetButton);
  }
});

$('.reset').on('click', function() {
  location.reload();
});


function calculateDistance() {
  let xDif = (selectedCity[0].x) - (selectedCity[1].x);
  let yDif = (selectedCity[0].y) - (selectedCity[1].y);
  dist = Math.round(Math.sqrt((xDif*xDif) + (yDif*yDif)));
  return dist*100;
}
